using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;
using Carrito.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Carrito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CestaController: ControllerBase
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public CestaController(DataContext db, IMapper mapper){
            _db = db;
            _mapper = mapper;
        }

        [HttpGet]        
        public async Task<ActionResult<List<CestaDTO>>> Get(){
            var cestaList = await _db.Cesta.ToListAsync();
            return _mapper.Map<List<CestaDTO>>(cestaList);
        }

        [HttpGet("{id}", Name = "CestaByID")]
        public async Task<ActionResult<List<ArticulosDTO>>> GetById(int id){
            int[] ids = new int[]{id};
            var data = from c in _db.Cesta
                        join a in _db.Articulos on c.id_articulo equals a.id
                        where ids.Contains(c.id_cliente)
                        select _mapper.Map<ArticulosDTO>(a);

            if(data == null){
                return NotFound();
            }
            var cestaList = await data.ToListAsync();

            return _mapper.Map<List<ArticulosDTO>>(cestaList);
        }

        [HttpPost]
        public async Task<ActionResult> Post(CestaCreateDTO cesta){
            var cestaDAO = _mapper.Map<Cesta>(cesta);
            cestaDAO.created_at = DateTime.Now;
            _db.Add(cestaDAO);
            await _db.SaveChangesAsync();
            var CestaDTO = _mapper.Map<Cesta>(cestaDAO);
            return new CreatedAtRouteResult("CestaById", new {id = cestaDAO.id}, CestaDTO);
        }

        [HttpDelete("{user}/{id}")]
        public async Task<ActionResult<int>> Delete(int user, int id){
            try{
                var cesta = await _db.Cesta.FirstOrDefaultAsync(c => c.id_articulo == id && c.id_cliente == user);
                _db.Cesta.Remove(cesta);
                _db.SaveChanges();
                return id;
            }catch(Exception e){
                return NotFound();            
            }

        }
    }
}