using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;
using Carrito.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClienteController: ControllerBase
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;
        
        public ClienteController(DataContext db, IMapper mapper){
            _db = db;
            _mapper = mapper;
        }

        [HttpGet]        
        public async Task<ActionResult<List<ClienteDTO>>> Get(){
            var clienteList = await _db.Cliente.ToListAsync();
            return _mapper.Map<List<ClienteDTO>>(clienteList);
        }

        [HttpGet("{id}", Name = "ClienteByID")]
        public async Task<ActionResult<ClienteDTO>> GetById(int id){
            var cliente = await _db.Cliente.FirstOrDefaultAsync(c => c.id == id);
            if(cliente == null){
                return NotFound();
            }

            return _mapper.Map<ClienteDTO>(cliente);
        }

        [HttpPost]
        public async Task<ActionResult> Post(ClienteCreateDTO cliente){
            var clienteDAO = _mapper.Map<Cliente>(cliente);
            _db.Add(clienteDAO);
            await _db.SaveChangesAsync();
            var ClienteDTO = _mapper.Map<Cliente>(clienteDAO);
            return new CreatedAtRouteResult("ClienteById", new {id = clienteDAO.id}, ClienteDTO);
        }

    }
}
