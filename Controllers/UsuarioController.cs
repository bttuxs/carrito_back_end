using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;
using Carrito.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class UsuarioController:ControllerBase
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public UsuarioController(DataContext db, IMapper mapper){
            _db = db;
            _mapper = mapper;
        }
        
        [HttpGet]        
        public async Task<ActionResult<List<UsuarioDTO>>> Get(){
            var usuarioList = await _db.Usuario.ToListAsync();
            return _mapper.Map<List<UsuarioDTO>>(usuarioList);
        }
        [HttpGet("{id}", Name = "UsuariosById")]
        public async Task<ActionResult<UsuarioDTO>> GetById(int id){
            var usuario = await _db.Usuario.FirstOrDefaultAsync(c => c.id == id);
            if(usuario == null){
                return NotFound();
            }

            return _mapper.Map<UsuarioDTO>(usuario);
        }
        [HttpPost]
        public async Task<ActionResult> Post(UsuarioCreateDTO usuario){
            var usuarioDAO = _mapper.Map<Usuario>(usuario);
            usuarioDAO.created_at = DateTime.Now;
            _db.Add(usuarioDAO);
            await _db.SaveChangesAsync();
            var usuarioDTO = _mapper.Map<UsuarioDTO>(usuarioDAO);
            return new CreatedAtRouteResult("UsuariosById", new {id = usuarioDAO.id}, usuarioDTO);
        }
        [HttpPost("login")]
        public async Task<ActionResult<UsuarioDTO>> GetLogin(UsuarioLoginDTO login){
            var usuario = await _db.Usuario.FirstOrDefaultAsync(c => c.usuario == login.usuario && login.password == c.password);
            if(usuario == null){
                return NotFound();
            }

            return _mapper.Map<UsuarioDTO>(usuario);
        }

    }
}