using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;
using Carrito.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ArticuloController: ControllerBase
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;
        
        public ArticuloController(DataContext db, IMapper mapper){
            _db = db;
            _mapper = mapper;
        }

        [HttpGet]        
        public async Task<ActionResult<List<ArticulosDTO>>> Get(){
            var articulosList = await _db.Articulos.ToListAsync();
            return _mapper.Map<List<ArticulosDTO>>(articulosList);
        }

        [HttpGet("{id}", Name = "ArticulosByID")]
        public async Task<ActionResult<ArticulosDTO>> GetById(int id){
            var articulos = await _db.Articulos.FirstOrDefaultAsync(c => c.id == id);
            if(articulos == null){
                return NotFound();
            }

            return _mapper.Map<ArticulosDTO>(articulos);
        }

        [HttpPost]
        public async Task<ActionResult> Post(ArticulosCreateDTO articulos){
            var articulosDAO = _mapper.Map<Articulos>(articulos);
            _db.Add(articulosDAO);
            await _db.SaveChangesAsync();
            var ArticulosDTO = _mapper.Map<Articulos>(articulosDAO);
            return new CreatedAtRouteResult("ArticulosById", new {id = articulosDAO.id}, ArticulosDTO);
        }
        
    }
}