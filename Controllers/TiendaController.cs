using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;
using Carrito.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TiendaController: ControllerBase
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public TiendaController(DataContext db, IMapper mapper){
            _db = db;
            _mapper = mapper;
        }
        [HttpGet]        
        public async Task<ActionResult<List<TiendaDTO>>> Get(){
            var tiendaList = await _db.Tienda.ToListAsync();
            return _mapper.Map<List<TiendaDTO>>(tiendaList);
        }

        [HttpGet("{id}", Name = "TiendaById")]
        public async Task<ActionResult<TiendaDTO>> GetById(int id){
            var tienda = await _db.Tienda.FirstOrDefaultAsync(c => c.id == id);
            if(tienda == null){
                return NotFound();
            }

            return _mapper.Map<TiendaDTO>(tienda);
        }

        [HttpPost]
        public async Task<ActionResult> Post(TiendaCreateDTO tienda){
            var tiendaDAO = _mapper.Map<Tienda>(tienda);
            _db.Add(tiendaDAO);
            await _db.SaveChangesAsync();
            var TiendaDTO = _mapper.Map<TiendaDTO>(tiendaDAO);
            return new CreatedAtRouteResult("TiendaById", new {id = tiendaDAO.id}, TiendaDTO);
        }

        [HttpPost("articulos")]
        public async Task<ActionResult> AssinmentProduct(ArticulosTiendaCreateDTO articulo){
            try{
                var articuloDAO = _mapper.Map<ArticulosTienda>(articulo);
                articuloDAO.created_at = DateTime.Now;
                _db.Add(articuloDAO);
                await _db.SaveChangesAsync();
                return Ok();
            }catch(Exception e) {
                Console.WriteLine(e.StackTrace);
                return NotFound();
            }

        }
    }
}