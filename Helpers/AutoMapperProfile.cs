using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Carrito.DTO;
using Carrito.Entity;

namespace Carrito.Helpers
{
    public class AutoMapperProfile: Profile
    {

        public AutoMapperProfile()
        {
            CreateMap<Tienda, TiendaDTO>().ReverseMap();
            CreateMap<Tienda, TiendaCreateDTO>().ReverseMap();

            CreateMap<Cliente, ClienteDTO>().ReverseMap();
            CreateMap<Cliente, ClienteCreateDTO>().ReverseMap();

            CreateMap<Articulos, ArticulosDTO>().ReverseMap();
            CreateMap<Articulos, ArticulosCreateDTO>().ReverseMap();

            CreateMap<Cesta, CestaDTO>().ReverseMap();
            CreateMap<Cesta, CestaCreateDTO>().ReverseMap();

            CreateMap<ArticulosTienda, ArticulosTiendaDTO>().ReverseMap();
            CreateMap<ArticulosTienda, ArticulosTiendaCreateDTO>().ReverseMap();

            CreateMap<Usuario, UsuarioDTO>().ReverseMap();
            CreateMap<Usuario, UsuarioCreateDTO>().ReverseMap();
            CreateMap<Usuario, UsuarioLoginDTO>().ReverseMap();
        }
    }
}