using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Carrito.Entity;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Helpers
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options)
        {

        }

        public DbSet<Tienda> Tienda { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Articulos> Articulos { get; set; }
        public DbSet<Cesta> Cesta { get; set; }
        public DbSet<ArticulosTienda> ArticulosTienda { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
    }
}