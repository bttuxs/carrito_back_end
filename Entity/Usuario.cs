using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Entity
{
    [Index(nameof(usuario), IsUnique = true)]
    public class Usuario
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int id { get; set; }
        [Required]
        public string usuario { get; set; } 
        [Required]
        public string password { get; set; }
        public int  id_cliente { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime created_at { get ; set  ; } =DateTime.Now;
    }
}