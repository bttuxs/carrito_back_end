using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Carrito.Entity
{
    [Index(nameof(codigo), IsUnique = true)]
    public class Articulos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int id { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public float precio { get; set; } 
        [Column(TypeName="image")]
        public string imagen { get; set;}
        public int stock { get; set; }  
    }
}