using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class TiendaCreateDTO
    {
        [Required]
        public string sucursal { get; set; }
        [Required]
        public string direccion { get; set; }        
    }
}