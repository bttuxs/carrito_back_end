using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class ArticulosDTO
    {
        public int id { get; set; }
        [Required]
        public string codigo { get; set; }
        [Required]
        public string Descripcion { get; set; }
        [Required]
        public float precio { get; set; } 
        public string imagen { get; set;}

        public int stock { get; set; }        
    }
}