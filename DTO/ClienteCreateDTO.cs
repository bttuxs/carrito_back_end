using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class ClienteCreateDTO
    {
        [Required]
        public string nombre {get; set;}
        [Required]
        public string apellidos {get; set;}
        [Required]
        public string direccion {get; set;}        
    }
}