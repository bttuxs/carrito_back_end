using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Carrito.DTO
{
    public class ClienteDTO
    {
        public int id { get; set; }
        [Required]
        public string nombre {get; set;}
        [Required]
        public string apellidos {get; set;}
        [Required]
        public string direccion {get; set;}
    }
}