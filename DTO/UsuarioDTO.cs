using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class UsuarioDTO
    {
        public int id { get; set; }
        public string usuario { get; set; } 
        public string password { get; set; }
        public int  id_cliente { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now;
    }
}