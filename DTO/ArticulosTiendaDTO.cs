using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class ArticulosTiendaDTO
    {
        public int id { get; set; }
        public int id_articulo { get; set;}
        public int id_tienda { get; set; }
        public DateTime created_at { get; set; } = DateTime.Now; 
    }
}