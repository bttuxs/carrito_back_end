using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace Carrito.DTO
{
    public class CestaDTO
    {
        public int id { get; set; }
        public int id_articulo { get; set;}
        public int id_cliente { get; set; }
        public DateTime created_at {get; set; }
    }
}