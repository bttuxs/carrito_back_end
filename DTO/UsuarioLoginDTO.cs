using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class UsuarioLoginDTO
    {
        [Required]
        public string usuario { get; set; } 
        [Required]
        public string password { get; set; }

    }
}