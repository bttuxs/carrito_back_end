using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class UsuarioCreateDTO
    {
        [Required]
        public string usuario { get; set; } 
        [Required]
        public string password { get; set; }
        public int  id_cliente { get; set; }
    }
}