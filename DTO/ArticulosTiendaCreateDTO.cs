using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Carrito.DTO
{
    public class ArticulosTiendaCreateDTO
    {   
        [Required]
        public int id_articulo { get; set;}
        [Required]
        public int id_tienda { get; set; }
    }
}